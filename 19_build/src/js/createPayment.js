import { el, setChildren } from 'redom';

export default function createPayment() {
  const $paymentContainer = document.createElement('div');
  $paymentContainer.classList.add('payment', 'flex-column');
  $paymentContainer.id = 'payment-container';

  const $paymentTitle = el('h2', 'Оплата');
  const $card = el('div', { class: 'card' });

  const $cardImage = el('div', { class: 'card__logo-wrapper' },
    el('img', {
      class: 'card__logo',
      id: 'card-logo',
    }));

  const $cardNumber = el('div', { class: 'card__number' }, [
    el('h5', 'Номер карты'),
    el('h6', { id: 'label-cardnumber' }, '0000 0000 0000 0000'),
  ]);

  const $cardWrapper = el('div', { class: 'card__wrapper' }, [
    el('div', { class: 'card__date' }, [
      el('h5', 'Действительна до:'),
      el('h6', { id: 'label-cardexpiration' }, '00 / 00'),
    ]),
    el('div', { class: 'card__cvc' }, [
      el('h5', 'CVC'),
      el('h6', { id: 'label-cvc' }, '000'),
    ]),
  ]);

  const $form = el('form', { class: 'card-form', id: 'form' });
  const $formNumber = el('div', { class: 'number' }, [
    el('label', { class: 'label' }, 'Номер карты'),
    el('input', {
      type: 'text',
      id: 'cardnumber',
      class: 'card-input',
      placeholder: '0000 0000 0000 0000',
    }),
  ]);

  const $formWrapper = el('div', { class: 'input-wrapper' }, [
    el('div', { class: 'date' }, [
      el('label', { class: 'label' }, 'Действительна до:'),
      el('input', {
        type: 'text',
        id: 'cardexpiration',
        class: 'card-input',
        placeholder: 'ДД / ГГ',
      }),
    ]),
    el('div', { class: 'cvc' }, [
      el('label', { class: 'label' }, 'CVC'),
      el('input', {
        type: 'text',
        id: 'cardcvc',
        class: 'card-input',
        placeholder: '123',
      }),
    ]),
  ]);

  const $formEmail = el('div', { class: 'email' }, [
    el('label', { class: 'label' }, 'Ваш E-mail. Мы пришлём чек.'),
    el('input', {
      type: 'text',
      id: 'email',
      class: 'card-input',
      placeholder: 'your@email.ru',
    }),
  ]);

  const $formBtn = el('button', {
    class: 'button',
    id: 'button',
    type: 'button',
    disabled: true,
  }, 'Введите данные');

  setChildren($form, [$formNumber, $formWrapper, $formEmail, $formBtn]);
  setChildren($card, [$cardImage, $cardNumber, $cardWrapper]);
  setChildren($paymentContainer, [$paymentTitle, $card, $form]);

  return $paymentContainer;
}
