import '@babel/polyfill';

import '../css/style.css';
import Inputmask from 'inputmask';
import EmailValidator from 'email-validator';
import {
  isValid,
  isExpirationDateValid,
  isSecurityCodeValid,
} from 'creditcard.js';
import createPayment from './createPayment';
import createOrder from './createOrder';

import Visa from '../img/visa.svg';
import MasterCard from '../img/mastercard.svg';
import Discover from '../img/discover.svg';
import JCB from '../img/jcb.svg';
import Mir from '../img/mir.svg';
import Error from '../img/error.svg';

const appContainer = document.createElement('div');
appContainer.classList.add('container', 'flex-row');

appContainer.append(createOrder());
appContainer.append(createPayment());
window.document.body.append(appContainer);

const $numberOnCard = document.getElementById('label-cardnumber');
const $dateOnCard = document.getElementById('label-cardexpiration');
const $cvcOnCard = document.getElementById('label-cvc');

const $cardNumber = document.getElementById('cardnumber');
const $cardDate = document.getElementById('cardexpiration');
const $cardCvc = document.getElementById('cardcvc');
const $email = document.getElementById('email');
const $cardLogo = document.getElementById('card-logo');
const $form = document.getElementById('form');
const $formBtn = document.getElementById('button');

$cardLogo.src = Error;

// Маскирование полей ввода

const maskNumber = new Inputmask('9999 9999 9999 9999');
const maskDate = new Inputmask('99/99');
const maskCvc = new Inputmask('999');

maskNumber.mask($cardNumber);
maskDate.mask($cardDate);
maskCvc.mask($cardCvc);

// Перенос данных из полей ввода в декоративную карту
$cardNumber.addEventListener('input', () => {
  if ($cardNumber.value) {
    $numberOnCard.textContent = $cardNumber.value;
  } else {
    $numberOnCard.textContent = '0000 0000 0000 0000';
  }
});

$cardDate.addEventListener('input', () => {
  if ($cardDate.value) {
    $dateOnCard.textContent = $cardDate.value;
  } else {
    $dateOnCard.textContent = '00 / 00';
  }
});

$cardCvc.addEventListener('input', () => {
  if ($cardCvc.value) {
    $cvcOnCard.textContent = $cardCvc.value;
  } else {
    $cvcOnCard.textContent = '000';
  }
});

// Валидация полей ввода

function checkValid() {
  if (
    $form.querySelectorAll('input').length
    === $form.querySelectorAll('input.is-valid').length
  ) {
    $formBtn.disabled = false;
    $formBtn.classList.add('button-cta');
    $formBtn.textContent = 'Оплатить';
  } else {
    $formBtn.disabled = true;
    $formBtn.classList.remove('button-cta');
    $formBtn.textContent = 'Введите данные';
  }
}

$cardNumber.addEventListener('blur', (e) => {
  if (e.target.value.trim() === '') {
    $cardLogo.src = Error;
    return;
  }

  const isNumberValid = isValid(e.target.value);

  if (isNumberValid) {
    $cardNumber.classList.add('is-valid');
    $cardNumber.classList.remove('not-valid');
  } else {
    $cardNumber.classList.remove('is-valid');
    $cardNumber.classList.add('not-valid');
  }
  const firstNumber = e.target.value.slice(0, 1);

  if (isNumberValid && firstNumber) {
    switch (firstNumber) {
      case '2':
        $cardLogo.src = Mir;
        break;
      case '3':
        $cardLogo.src = JCB;
        break;
      case '4':
        $cardLogo.src = Visa;
        break;
      case '5':
        $cardLogo.src = MasterCard;
        break;
      case '6':
        $cardLogo.src = Discover;
        break;
      default:
        $cardLogo.src = Error;
    }
  } else {
    $cardLogo.src = Error;
  }

  checkValid();
});

$cardDate.addEventListener('blur', (e) => {
  if (e.target.value.trim() === '') return;
  const value = $cardDate.value.split('/');
  const isDateValid = isExpirationDateValid(value[0], value[1]);

  if (!isDateValid) {
    $cardDate.classList.add('not-valid');
    $cardDate.classList.remove('is-valid');
  } else {
    $cardDate.classList.remove('not-valid');
    $cardDate.classList.add('is-valid');
  }
  checkValid();
});
$cardCvc.addEventListener('blur', (e) => {
  if (e.target.value.trim() === '') return;
  const isCvcValid = isSecurityCodeValid($cardNumber.value, e.target.value);
  if (isCvcValid) {
    $cardCvc.classList.remove('not-valid');
    $cardCvc.classList.add('is-valid');
  } else {
    $cardCvc.classList.add('not-valid');
    $cardCvc.classList.remove('is-valid');
  }
  checkValid();
});

$email.addEventListener('blur', (e) => {
  if (e.target.value.trim() === '') return;
  const isEmailValid = EmailValidator.validate(e.target.value);
  if (isEmailValid) {
    $email.classList.remove('not-valid');
    $email.classList.add('is-valid');
  } else {
    $email.classList.add('not-valid');
    $email.classList.remove('is-valid');
  }
  checkValid();
});
