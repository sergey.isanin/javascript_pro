import { el, setChildren } from "redom";
import Kot from "../img/kot.jpg";

export default function createOrder() {
  const $orderContainer = document.createElement("div");
  $orderContainer.classList.add("order", "flex-column");

  const $orderWrapper = el("div", { class: "order__wrapper" }, [
    el("h2", "Ваш заказ"),
    el("img", {
      class: "order__img",
      src: Kot,
      alt: "Фото кота",
    }),
    el("h5", { class: "order__description" }, "Котёнок - 1 шт."),
  ]);

  const $orderPrice = el("h2", "Итого: 100 000 P");

  setChildren($orderContainer, [$orderWrapper, $orderPrice]);

  return $orderContainer;
}
