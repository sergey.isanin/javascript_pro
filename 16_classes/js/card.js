export default class Card {
  // _ переменная, предназначенная для использования только внутри объекта
  _open = false;
  _success = false;

  constructor(container, number, action) {
    this.card = document.createElement("div"); //this. обращение к свойству
    this.card.classList.add("card");
    this.card.textContent = number;
    this.number = number;

    this.card.addEventListener("click", () => {
      if (this.open == false && this.success == false) {
        this.open = true;
        action(this);
      }
    });
    container.append(this.card);
  }
  // открытая карточка
  set open(value) {
    this._open = value;
    value
      ? this.card.classList.add("open")
      : this.card.classList.remove("open"); //тернарный оператор
  }
  get open() {
    return this._open;
  }
  // зеленая карточка
  set success(value) {
    this._success = value;
    value
      ? this.card.classList.add("success")
      : this.card.classList.remove("success"); //тернарный оператор
  }
  get success() {
    return this._open;
  }
}
