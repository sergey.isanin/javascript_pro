import Card from "./card.js";

function newGame(container, cardsCount) {
  //Игровое поле
  let cardsNumberArray = [],
    cardsArray = [],
    firstCard = null,
    secondCard = null;
  for (let i = 1; i <= cardsCount / 2; i++) {
    cardsNumberArray.push(i);
    cardsNumberArray.push(i);
  }

  cardsNumberArray = cardsNumberArray.sort(() => Math.random() - 0.5);

  for (const cardsNumber of cardsNumberArray) {
    cardsArray.push(new Card(container, cardsNumber, flip));
    console.log(cardsNumber, cardsNumberArray);
    // console.log(flip);
  }

  // Логика
  function flip(card) {
    // если карточки не совпали то сброс
    if (firstCard !== null && secondCard !== null) {
      console.log("если карточкии не совпали то сбросс");
      console.log(firstCard.number, secondCard.number);
      if (firstCard.number != secondCard.number) {
        firstCard.open = false;
        secondCard.open = false;
        firstCard = null;
        secondCard = null;
      }
    }

    if (firstCard == null) {
      firstCard = card;
    } else {
      if (secondCard == null) {
        secondCard = card;
      }
    }
    // зелёные карточкии
    if (firstCard !== null && secondCard !== null) {
      console.log("зелёные карточкии");
      console.log(firstCard.number, secondCard.number);
      if (firstCard.number == secondCard.number) {
        firstCard.success = true;
        secondCard.success = true;
        firstCard = null;
        secondCard = null;
      }
    }

    if (
      document.querySelectorAll(".card.success").length ==
      cardsNumberArray.length
    ) {
      alert("Победа!");
      // Сброс
      container.innerHTML = "";
      cardsNumberArray = [];
      cardsArray = [];
      firstCard = null;
      secondCard = null;
      newGame(container, cardsCount);
    }
  }
}
newGame(document.getElementById("game"), 6);
