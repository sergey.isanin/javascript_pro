export function render(data) {
  const container = document.createElement("div");
  container.classList.add(
    "container",
    "d-flex",
    "justify-content-between",
    "py-4",
    "flex-wrap"
  );
  for (const product of data.products) {
    const productCard = document.createElement("div");
    const image = document.createElement("img");
    const cartBody = document.createElement("div");
    const title = document.createElement("h5");
    const price = document.createElement("p");

    productCard.style.width = "18%";
    productCard.classList.add("card", "my-2");
    image.classList.add("card-img-top");
    cartBody.classList.add("card-body");
    title.classList.add("card-title");
    price.classList.add("card-text");

    productCard.append(image);
    productCard.append(cartBody);
    cartBody.append(title);
    cartBody.append(price);

    image.src = product.image;
    image.alt = product.name;
    title.textContent = product.name;
    price.textContent = product.price;

    container.append(productCard);
  }

  return container;
}
