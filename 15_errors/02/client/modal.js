export function createModal(text) {
  const containerModal = document.createElement("div");
  const boxModal = document.createElement("div");
  const bodyModal = document.createElement("div");

  containerModal.classList.add(
    "align-items-center",
    "text-white",
    "bg-danger",
    "border-0",
    "position-relative"
  );
  containerModal.style.width = "350px";
  containerModal.style.left = "70%";
  containerModal.style.top = "100%";
  containerModal.style.opacity = "10";
  containerModal.role = "alert";
  containerModal.ariaLive = "assertive";
  containerModal.ariaAtomic = "true";
  boxModal.classList.add("d-flex");
  bodyModal.classList.add("toast-body");

  containerModal.append(boxModal);
  boxModal.append(bodyModal);

  bodyModal.textContent = text;

  return containerModal;
}
