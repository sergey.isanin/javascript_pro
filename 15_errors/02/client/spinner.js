export function createSpinner() {
  const spinnerContainer = document.createElement("div");
  const spinnerBox = document.createElement("div");
  const spinner = document.createElement("span");

  spinnerContainer.classList.add(
    "justify-content-center",
    "align-items-center"
  );
  spinnerContainer.style.height = "500px";
  spinnerContainer.style.display = "flex";
  spinnerBox.classList.add("spinner-border", "text-primary");
  spinnerBox.style.width = "100px";
  spinnerBox.style.height = "100px";
  spinner.classList.add("sr-only");

  spinnerBox.role = "status";

  spinnerContainer.append(spinnerBox);
  spinnerBox.append(spinner);

  return spinnerContainer;
}
