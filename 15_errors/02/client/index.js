import { createSpinner } from "./spinner.js";
import { render } from "./product-list.js";
import { createModal } from "./modal.js";

const spinner = createSpinner();

const appContainer = document.getElementById("app");

let i = 1;
async function loadResource() {
  const response = await fetch("http:api/products?status=200");
  if (response.status === 500) {
    if (i < 3) {
      ++i;
      return loadResource();
    }
    const error = new Error();
    error.status = 500;
    throw error;
  }
  if (response.status === 404) {
    const error = new Error();
    error.status = 404;
    throw error;
  }
  const result = await response.json();
  return result;
}

const modal = createModal("Идет загрузка");
appContainer.append(spinner);
appContainer.append(modal);

async function rendalPage() {
  try {
    const product = await loadResource();
    appContainer.prepend(render(product));
  } catch (error) {
    if (error.status === 500) {
      appContainer.append(
        createModal("Произошла ошибка, попробуйте обновить страницу позже")
      );
    }
    if (error.status === 404) {
      const h2 = document.createElement("h2");
      h2.innerHTML = "Список товаров пуст";
      appContainer.append(h2);
    }
  } finally {
    spinner.remove();
    setTimeout(() => modal.remove(), 3000);
  }
}
const off = createModal("Произошла ошибка, проверьте подключение к интернету");
rendalPage();
window.addEventListener("offline", () => {
  appContainer.append(off);
});

window.addEventListener("online", () => {
  off.remove();
});
