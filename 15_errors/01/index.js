// 1. Функция calculateDiscount должна выбрасывать ошибку, если её аргументы не числа.
export function calculateDiscount(price, percent) {
  if (typeof price !== "number" || typeof percent !== "number") {
    const err = new TypeError();
    throw err;
  }
  return (price / 100) * percent;
}

// 2. Функция getMarketingPrice не должна выбрасывать ошибку,
// если в структуре товара нет информации про цены.
export function getMarketingPrice(product) {
  const productObject = JSON.parse(product);
  if(!productObject || !productObject.prices) {
    return null
  }
  return productObject.prices.marketingPrice
}

// 3. Асинхронная функция loadAvatar должна возвращать
// дефолтную картинку пользователя, а не выбрасывать исключение.
// Функция имитирует неудачный запрос за картинкой
function fetchAvatarImage(userId) {
  return new Promise((resolve, reject) => {
    reject(new Error(`Error while fetching image for user with id ${userId}`));
  });
}

export async function getAvatarUrl(userId) {
  try {
    const image = await fetchAvatarImage(userId);
    return image.url;
  } catch (err) {
    return "/images/default.jpg";
  }
}
