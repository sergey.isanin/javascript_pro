const goTopBtn = document.querySelector('.scroll-top');

goTopBtn.addEventListener('click', () => {
    if (window.pageYOffset > 0) {
        window.scrollTo({ top: 0, behavior: 'smooth' })
    }
});

window.addEventListener('scroll', () => {
    const scrolled = window.pageYOffset;
    if (scrolled > 100) {
      goTopBtn.classList.add("go-top--show");
    } else {
      goTopBtn.classList.remove("go-top--show");
    }
  
}, { passive: true })
