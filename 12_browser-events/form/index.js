
    const inputArray = document.querySelectorAll('.form-control');
    const button = document.querySelector('.btn');
    const container = document.querySelector('.container')

    inputArray.forEach(input => input.addEventListener('blur', () => {
        let str = input.value
            .trim()
            .replace(/[^a-zа-яё0-9\s-]/gi, '')
            .replace(/\s+/gi, ' ')
            .replace(/-+/gi, '-');
        let str1 = str.substr(0, 1).toUpperCase() + str.substr(1).toLowerCase();
        input.value = str1;
    }));

    button.addEventListener('click', (event) => {
        event.preventDefault();
        inputArray.forEach(input => {
            var para = document.createElement("p");
            var newText = document.createTextNode(input.value);
            para.appendChild(newText);
        
            container.appendChild(para);
            input.value = '';
        })
    });
