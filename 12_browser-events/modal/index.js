document.addEventListener('DOMContentLoaded', () => {
    const openButton = document.querySelector('.js-open-modal');
    const modal = document.querySelector(openButton.dataset.target);

    openButton.addEventListener('click', () => {
        modal.style.display = 'block';
    });

    modal.querySelector('.modal-dialog').addEventListener('click', (event) => {
        event.stopPropagation();
    });

    modal.addEventListener('click', () => {
        modal.style.display = 'none';
    })
});